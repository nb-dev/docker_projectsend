# About ProjectSend 

ProjectSend (previously cFTP) is a free, clients-oriented, private file
sharing web application.

Clients are created and assigned a username and a password. Then you can
upload as much files as you want under each account, and optionally add
a name and description to them. 

ProjectSend is hosted on Google Code.

Feel free to participate!

Main website:
http://www.projectsend.org/

Project:
http://code.google.com/p/clients-oriented-ftp/

Translations:
https://www.transifex.com/projects/p/projectsend/

--------------------------------------------------------------------------------------------

Questions, ideas? Want to join the project?
Send your message to contact@projectsend.org or join us on Facebook, on
https://www.facebook.com/pages/ProjectSend/333455190044627

--------------------------------------------------------------------------------------------

# About this image

Even ProjectSend is very easy to install on an existing HTTPd, it might be a big challenge if there isn't such a server
yet. Also, company rules may forbid the installation of the software together with your main-business-application, when
no company is there to take the credibility for bugs or support. Or maybe you just like to run your applications inside
sandboxes to prevent unveiling of other data if there is a security issue in the code you run on your servers. Or you
like the idea to test without any impact on your systems; no libraries or programs, laying around decades after you have
removed the application from your host, ...

There are dozens of good reasons for this Image, so here it is :)

# Getting started

Just install [Docker](https://www.docker.com/). Afterwards, start the application with `docker run -p 80:80 -e
DB_NAME=YOUR_DB_NAME -e DB_HOST=YOUR_MYSQL_SERVER -e DB_USER=YOUR_MYSQL_USER -e DB_PASS=YOUR_MYSQL_PASSWORD -d
derjudge/projectsend` and you are done! You will need an existing MySQL server for this to work.

If you do not have an already existing MySQL server, [Docker](https://www.docker.com/) is coming to the rescue once more:
Start an MariaDB (MySQL in good) instance like this: `docker run --name ps_mariadb -e MYSQL_ROOT_PASSWORD=YOUR_MYSQL_ROOT_PW
-d mariadb`. After this (MySQL/MariaDB container must run before you start ProjectSend container!), start the ProjectSend
container as described above, but add `--link=mariadb:mysql` to the command line.

## Example for running MySQL/MariaDB and ProjectSend using Docker

1. `docker run --name ps_mariadb -e MYSQL_ROOT_PASSWORD=123456 -d mariadb`
2. `docker run -p 80:80 -e DB_NAME=ps -e DB_HOST=mysql -e DB_USER=root -e DB_PASS=123456 --link=mariadb:mysql -d derjudge/projectsend`

I consider it your responsibility to choose a good password instead of `123456`! But make sure, you define the same ones
for the two containers!

# Detailed Docs

TODO
